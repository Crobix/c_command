#include <stdio.h>
#include <stdlib.h>
#include <mem.h>
#include <stdbool.h>
#include "tribulle.h"

void affichage(const float *tab, int taille) {
    for (int i = 0; i < taille; ++i) {
        printf("%f \n", *(tab + i));
    }
}

int main(int argc, char **argv) {
    char *prt;
    char **prt2 = argv + 3;
    float *tab = malloc(sizeof(float) * argc);
    bool dsc = false;

    prt = *(argv + 1);
    if (strcmp(prt, "sort") != 0) {
        return EXIT_FAILURE;
    }

    prt = *(argv + 2);
    if (strcmp(prt, "-d") == 0) {
        dsc = true;
    }

    if (argc >= 3) {
        float *t = tab;
        for (int i = 3; i < argc; ++i) {
            *t = (float) atof(*prt2);
            t++;
            prt2++;
        }

        tri_bulle_pointeur((float **) tab, argc - 3, dsc);
        affichage(tab, argc - 3);
    }
    return EXIT_SUCCESS;
}