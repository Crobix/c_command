//
// Created by Loic on 14/03/2019.
//

#include <stdbool.h>
#include "tribulle.h"

void tri_bulle_pointeur(float *tab[], int taille, bool asc) {
    bool is_sorted;
    int index = 0;
    float *p, *q;

    do {
        p = (float *) tab;
        q = (float *) (tab + 1);
        is_sorted = true;
        index = 0;

        while (index < taille - 1) {
            if ((asc && *p < *q) || (!asc && *p > *q)) {
                float swap = *p;
                *p = *q;
                *q = swap;
                is_sorted = false;
            }

            index++;
            p++;
            q++;
        }
    } while (!is_sorted);
}